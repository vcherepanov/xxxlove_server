package main

import (
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"xxxadult_server/internal/entity"

	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/jackc/pgx/v5"
	"github.com/julienschmidt/httprouter"
)

var pool *pgxpool.Pool
var ctx = context.TODO()

func init() {
	dev := flag.Bool("dev", false, "use the development mode")
	flag.Parse()
	var dbURI string
	if *dev {
		dbURI = "postgres://vlad:1Cherepanov@localhost:5432/vlad"
	} else {
		dbURI = "postgres://xxxlove:1XXXLOVE@localhost:5432/xxxlove"
	}

	var err error
	pool, err = pgxpool.Connect(ctx, dbURI)
	if err != nil {
		log.Fatal(err)
	}
}

func GetAllVideos(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	ctx := context.TODO()

	// Получаем параметры пагинации из запроса
	pageParam := r.URL.Query().Get("page")
	limitParam := r.URL.Query().Get("limit")

	// Устанавливаем значения по умолчанию
	page := 1
	limit := 10

	// Преобразуем параметры в int
	if pageParam != "" {
		page, _ = strconv.Atoi(pageParam)
	}
	if limitParam != "" {
		limit, _ = strconv.Atoi(limitParam)
	}

	// Вычисляем смещение
	offset := (page - 1) * limit

	// Получаем общее количество записей
	var totalRecords int
	err := pool.QueryRow(ctx, "SELECT COUNT(*) FROM videos").Scan(&totalRecords)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	query := fmt.Sprintf("SELECT id, path, title, domain FROM videos LIMIT %d OFFSET %d", limit, offset)
	rows, err := pool.Query(ctx, query)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	defer rows.Close()

	var results []entity.Video
	for rows.Next() {
		var video entity.Video
		err := rows.Scan(&video.ID, &video.Path, &video.Title, &video.Domain)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(err.Error()))
			return
		}
		results = append(results, video)
	}

	// Формируем ответ с данными и информацией о пагинации
	response := struct {
		Videos       []entity.Video `json:"videos"`
		TotalRecords int            `json:"total"`
	}{
		Videos:       results,
		TotalRecords: totalRecords,
	}

	w.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(response)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
	}
}

func GetVideo(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	ctx := context.TODO()
	id := ps.ByName("id")
	fmt.Print(id)
	var video entity.Video
	err := pool.QueryRow(ctx, "SELECT id, path, title, domain FROM videos WHERE id=$1", id).Scan(&video.ID, &video.Path, &video.Title, &video.Domain)
	if err != nil {
		if err == pgx.ErrNoRows {
			w.WriteHeader(http.StatusNoContent)
			w.Write([]byte("No video found with given ID"))
			return
		}
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(video)
}

func main() {
	router := httprouter.New()
	router.GET("/api/videos", GetAllVideos)
	router.GET("/api/videos/:id", GetVideo)

	router.GlobalOPTIONS = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.Header.Get("Access-Control-Request-Method") != "" {
			header := w.Header()
			header.Set("Access-Control-Allow-Methods", header.Get("Allow"))
			header.Set("Access-Control-Allow-Origin", "*")
		}

		// Adjust status code to 204
		w.WriteHeader(http.StatusNoContent)
	})

	log.Fatal(http.ListenAndServe(":3001", router))
}
