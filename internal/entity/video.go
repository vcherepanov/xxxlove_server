package entity

type Video struct {
	ID     int    `json:"id"`
	Path   string `json:"path"`
	Title  string `json:"title"`
	Domain string `json:"domain"`
}
